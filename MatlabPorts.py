#*******************************************************
# * Valerio Giuffrida <v.giuffrida@ed.ac.uk> 2019
# * 
# * This file is part of the project ROTATION INVARIANT RESTRICTED BOLTZMANN MACHINE
# * The code below is a re-implementaition of some part of the code in https://github.com/kihyuks/icml2012_tirbm
# * 
# * This code is released under GPL v3. Please cite the reference paper when using it.
# *******************************************************

import numpy as np
from sys import float_info

def fspecial_gaussian(shape=(3,3),sigma=0.5):
    """
    2D gaussian mask - should give the same result as MATLAB's
    fspecial('gaussian',[shape],[sigma])
    """
    m,n = [(ss-1.)/2. for ss in shape]
    y,x = np.ogrid[-m:m+1,-n:n+1]
    h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
    h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h

def RotationMatrix(ws, angle):

    cX = ws / 2
    cY = ws / 2
    T = np.zeros((ws**2,ws**2))

    for i in range(1,ws+1):
        relY = i - cY - 0.5

        for j in range(1,ws+1):
            relX = j - cX - 0.5
            id = i + (j - 1) * ws

            xPrime = relX * np.cos(angle) + relY * np.sin(angle)
            yPrime = -1 * relX * np.sin(angle) + relY * np.cos(angle)

            xPrime = xPrime + cX + 0.5
            yPrime = yPrime + cY + 0.5

            q12x = np.floor(xPrime)
            q12y = np.floor(yPrime)
            q12x = max(1, q12x)
            q12y = max(1, q12y)
            q12x = min(ws, q12x)
            q12y = min(ws, q12y)
            q22x = np.ceil(xPrime)
            q22y = q12y
            q22x = min(ws, q22x)
            q22x = max(1, q22x)
            q11x = q12x
            q11y = np.ceil(yPrime)
            q11y = min(ws, q11y)
            q11y = max(1, q11y)
            q21x = np.ceil(q22x)
            q21y = q11y

            q11 = int((q11x - 1) * ws + q11y)
            q12 = int((q12x - 1) * ws + q12y)
            q21 = int((q21x - 1) * ws + q21y)
            q22 = int((q22x - 1) * ws + q22y)


            if (q21x == q11x):
                factor1 = 1
                factor2 = 0
            else:
                factor1 = (q21x - xPrime) / (q21x - q11x)
                factor2 = (xPrime - q11x) / (q21x - q11x)


            if (q12y == q11y):
                factor3 = 1
                factor4 = 0
            else:
                factor3 = (q12y - yPrime) / (q12y - q11y)
                factor4 = (yPrime - q11y) / (q12y - q11y)

            if (q21x != q11x) and (q12y != q11y):
                T[id-1, q11-1] = factor1 * factor3
                T[id-1, q21-1] = factor2 * factor3
                T[id-1, q12-1] = factor1 * factor4
                T[id-1, q22-1] = factor2 * factor4
            elif (q21x == q11x) and (q12y != q11y):
                T[id-1, q11-1] = factor1 * factor3
                T[id-1, q12-1] = factor1 * factor4
            elif (q21x != q11x) and (q12y == q11y):
                T[id-1, q11-1] = factor1 * factor3
                T[id-1, q21-1] = factor2 * factor3
            else:
                T[id-1, q11-1] = factor1 * factor3

    return T