# Rotation Invariant Restricted Boltzmann Machine

[![enter image description here](https://img.shields.io/badge/licence-GPL%203-green.svg)](https://www.gnu.org/licenses/gpl-3.0.html)

## Requirements

 - Python 2.x
 - Theano 1.0.2
 - numpy 1.14
 - script 1.1
 - matplotlib 2.2
 - progressbar2 3.38
 - [libsvm](https://www.csie.ntu.edu.tw/~cjlin/libsvm/) (copy inside a folder called *libsmv-master* and compile the python wrapper using the provided makefile)

## Quick start

You can run the bash script *./training.sh* and you will train the network for the mnist-rot, mpeg7, and Zalando-rot datasets.

## Command Line Overview

    --model, -m    : pretrained model to load and/or filename of the trained model (used for testing)
    --train        : train the network (if model is provided, it will use the given model as initialisation)
    --test         : test the network (a model should be provided if training is not performed)
    --hidden, -h   : number of hidden units (default is 500)
    --epochs, -e   : Number of epochs (default is 150)
    --rotations, -r: Number of rotations (default is 4)
    --with-cr, -C  : weight for the class regulariser (default is 0 - regulariser disabled -)


### Example

    python main_zalando_rot.mat --model trained_model.npz --epochs 100 --rotations 8 --hidden 500 -C 100 --train --test 

Similar setups can be used for the other datasets.

## Licence
All the code in this repository is released under the [GPL v3 licence](https://www.gnu.org/licenses/gpl-3.0.html). 

## Legacy code

A MATLAB implementation of our previous paper can be found here: https://bitbucket.org/teamrbm/rbm/
The code in that repository is not up-to-date and refers to our previous ICANN work.

