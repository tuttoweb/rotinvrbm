
#*******************************************************
# * Valerio Giuffrida <v.giuffrida@ed.ac.uk> 2019
# * 
# * This file is part of the project ROTATION INVARIANT RESTRICTED BOLTZMANN MACHINE
# * 
# * This code is released under GPL v3. Please cite the reference paper when using it.
# *******************************************************

from network import IRIRBM
import sys
sys.path.append('libsvm-master/python')
from svmutil import *
from scipy.io import loadmat
import numpy as np

import argparse

parser = argparse.ArgumentParser(description='IRI RBM')
parser.add_argument('--model','-m',dest='model',action='store',type=str,default=None)
parser.add_argument('--train',dest='train',action='store_true')
parser.add_argument('--test',dest='test',action='store_true')
#parser.add_argument('--use-rotations',dest='use_rot',action='store_true')
parser.add_argument('--epochs','-e',dest='epochs',action='store',type=int,default=150)
parser.add_argument('--with-orth','-O',dest='orth_reg',action='store',type=float,default=0)
parser.add_argument('--with-itr','-I',dest='itr_reg',action='store',type=float,default=0)
parser.add_argument('--with-cr','-C',dest='class_reg',action='store',type=float,default=0)
parser.add_argument('--rotations','-r',dest='n_rot',action='store',type=int,default=4)
parser.add_argument('--track','-t',dest='track',action='store',type=str,default=None)
parser.add_argument('--no-sh','-N',dest='sharing_grads',action='store_false')
parser.add_argument('--hidden',dest='h',action='store',type=int,default=500)
args = parser.parse_args()

data = loadmat('mpeg_dataset.mat')

S = args.n_rot

X = data['X'].reshape((28**2,-1)).transpose()
Y = data['Y']

indices = np.asarray(np.arange(Y.shape[1])).reshape((-1,2)).transpose()

x_train = X[indices[0,:],:]
y_train = Y[0,indices[0,:]].flatten()

x_test = X[indices[1,:],:]
y_test = Y[0,indices[1,:]].flatten()




a = IRIRBM(28**2,hidden_size=args.h,ws=28,n_rot=S)
a.OrthogonalRegulariser = args.orth_reg
a.ClassRegulariser = args.class_reg
a.Epochs=args.epochs
a.Eta=0.003
a.InformationTheoreticRegulariser=args.itr_reg
a.L2Regulariser=0.0001
a.TrackRotations = args.track
a.SharingGradients = args.sharing_grads
print "Sharing gradients: %s" % a.SharingGradients
a.compile(real=False)


if (args.model is not None):
    fname = args.model
    try:
        a.load(fname)
    except IOError:
        from sys import stderr
        stderr.write("File %s not found. Forcing training\n"%fname)
        args.train=True
else:
    fname = 'mpeg.npz'

if (args.train):
    a.train(x_train,display=True)
    a.save(fname)


if (args.test):
    x_train_features = a.test(x_train).tolist()
    x_test_features = a.test(x_test).tolist()

    y_test = y_test.tolist()
    y_train = y_train.tolist()

    training_problem = svm_problem(y_train,x_train_features)
    params = svm_parameter('-s 0 -t 2 -c 100')

    model = svm_train(training_problem,params)

    y_hat_train = svm_predict(y_train,x_train_features,model)
    y_hat_test  = svm_predict(y_test, x_test_features, model)

    #print "Train accuracy: %.5f" % np.mean(y_hat_train == y_train)
    #print "Train accuracy: %.5f" % np.mean(y_hat_test == y_test)

