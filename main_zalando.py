#*******************************************************
# * Valerio Giuffrida <v.giuffrida@ed.ac.uk> 2019
# * 
# * This file is part of the project ROTATION INVARIANT RESTRICTED BOLTZMANN MACHINE
# * 
# * This code is released under GPL v3. Please cite the reference paper when using it.
# *******************************************************

from network import IRIRBM
import sys
sys.path.append('libsvm-master/python')
from svmutil import *
from scipy.io import loadmat
import numpy as np

import argparse

parser = argparse.ArgumentParser(description='IRI RBM')
parser.add_argument('--model','-m',dest='model',action='store',type=str,default=None)
parser.add_argument('--train',dest='train',action='store_true')
parser.add_argument('--val',dest='val',action='store_true')
parser.add_argument('--test',dest='test',action='store_true')
parser.add_argument('--hidden',dest='h',action='store',type=int,default=500)
parser.add_argument('--use-rotations',dest='use_rot',action='store_true')
parser.add_argument('--epochs','-e',dest='epochs',action='store',type=int,default=150)
parser.add_argument('--with-orth','-O',dest='orth_reg',action='store',type=float,default=0)
parser.add_argument('--with-itr','-I',dest='itr_reg',action='store',type=float,default=0)
parser.add_argument('--with-cr','-C',dest='class_reg',action='store',type=float,default=0)
parser.add_argument('--rotations','-r',dest='n_rot',action='store',type=int,default=4)
parser.add_argument('--track','-t',dest='track',action='store',type=str,default=None)
parser.add_argument('--no-sh','-N',dest='sharing_grads',action='store_false')
args = parser.parse_args()

data = loadmat('my_zalando_rot.mat')

S = args.n_rot

def convert_rotations(t,T=4):
    ranges = np.linspace(0, 2 * np.pi, T+1)
    tt = np.zeros_like(t)

    for j in range(T):
        tt[ (ranges[j]<=t) * (t<ranges[j+1])] = j

    return tt[:,0]

x_train = data['Dataset'][0][0][0]
y_train = data['Dataset'][0][0][1][:,0]
t_train = data['Dataset'][0][0][2]

x_test  = data['Dataset'][0][0][3]
y_test  = data['Dataset'][0][0][4][:,0]
t_test  = data['Dataset'][0][0][5]

x_val  = data['Dataset'][0][0][6]
y_val  = data['Dataset'][0][0][7][:,0]
t_val  = data['Dataset'][0][0][8]

tt_train = convert_rotations(t_train,S)
tt_test  = convert_rotations(t_test,S)
tt_val   = convert_rotations(t_val,S)

x_train = x_train.reshape((28**2,-1)).transpose()
x_test  = x_test.reshape((28**2,-1)).transpose()
x_val   = x_val.reshape((28**2,-1)).transpose()



a = IRIRBM(28**2,hidden_size=500,ws=28,n_rot=S)
a.OrthogonalRegulariser = args.orth_reg
a.ClassRegulariser = args.class_reg
a.Epochs=args.epochs
a.Eta=0.003
a.InformationTheoreticRegulariser=args.itr_reg
a.L2Regulariser=0.0001
a.TrackRotations = args.track
a.SharingGradients = args.sharing_grads
print "Sharing gradients: %s" % a.SharingGradients
a.compile(real=False)

if (not args.use_rot):
    tt_train = None
    tt_test  = None
    tt_val   = None

if (args.model is not None):
    fname = args.model
    try:
        a.load(fname)
    except IOError:
        from sys import stderr
        stderr.write("File %s not found. Forcing training\n"%fname)
        args.train=True
else:
    fname = 'zaland_rot.npz'

if (args.train):
    a.train(x_train,tt_train,display=False)
    a.save(fname)


if (args.test or args.val):
    x_train_features = a.test(x_train,tt_train).tolist()
    x_test_features = a.test(x_test,tt_test).tolist() if args.test else a.test(x_val,tt_val).tolist()#a.test(x_test,tt_test).tolist()

    y_test = y_test.tolist() if args.test else y_val.tolist() #y_test.tolist()
    y_train = y_train.tolist()

    training_problem = svm_problem(y_train,x_train_features)
    params = svm_parameter('-s 0 -t 2 -c 100 -g 0.02')

    model = svm_train(training_problem,params)

    y_hat_train = svm_predict(y_train,x_train_features,model)
    y_hat_test  = svm_predict(y_test, x_test_features, model)

    #print "Train accuracy: %.5f" % np.mean(y_hat_train == y_train)
    #print "Train accuracy: %.5f" % np.mean(y_hat_test == y_test)

