#*******************************************************
# * Valerio Giuffrida <v.giuffrida@ed.ac.uk> 2019
# * 
# * This file is part of the project ROTATION INVARIANT RESTRICTED BOLTZMANN MACHINE
# * 
# * This code is released under GPL v3. Please cite the reference paper when using it.
# *******************************************************

import theano as th
import theano.tensor as T
from theano.ifelse import  ifelse
from theano.tensor.shared_randomstreams import RandomStreams
import numpy as np
import progressbar as pb
from MatlabPorts import RotationMatrix
import matplotlib.pyplot as plt
from warnings import warn

import sys
sys.path.append('libsvm-master/python')
from svmutil import *

from sys import stdout

def my_print_fn(op, xin):
    for attr in op.attrs:
        temp = getattr(xin, attr)
        if callable(temp):
            pmsg = temp()
        else:
            pmsg = temp
        print(op.message, attr, '=', pmsg)

debug = lambda t,s: T.printing.Print("Debug ["+s+"]",attrs=["__str__"],global_fn=my_print_fn)(t)

class IRIRBM(object):
    MiniBatchSize=128
    Epochs=150
    EtaSigma = 0.01
    Eta = 0.01
    SparsityTarget=0.1
    SparsityLambda=3
    L2Regulariser = 0.0001
    OrthogonalRegulariser = 0.001
    InformationTheoreticRegulariser=0.001
    EtaDecay = 0.01
    R = []
    Momentum={'initial': 0.5,'final': 0.9}
    ClassRegulariser=0
    ClassPrior = []
    TrackRotations = None
    SharingGradients=True
    UseTensor=True


    def __init__(this,input_size,n_rot=4,hidden_size=1000,ws=6,ch=1,shared=False,use_tensor=True):
        #lim = np.sqrt(6./(input_size+hidden_size));this.W = th.shared(np.random.uniform(-lim, lim, (input_size, hidden_size, n_rot)), 'W', allow_downcast=True)

        this.UseTensor = use_tensor

        stdev = np.sqrt(2. / (input_size + hidden_size))

        if (this.UseTensor):
        	this.W = th.shared(np.asarray(np.random.normal(0, stdev, (input_size, hidden_size, n_rot))), 'W', allow_downcast=True)
        else:
        	this.W = th.shared(np.asarray(np.random.normal(0, stdev, (input_size, hidden_size))), 'W', allow_downcast=True)
        #this.W = th.shared( 0.02*np.random.randn(input_size,hidden_size,n_rot),'W',allow_downcast=True)

        this.b = th.shared( np.zeros((hidden_size,)),'b',allow_downcast=True)
        this.c = th.shared(np.zeros((input_size,)), 'c', allow_downcast=True)
        this.sigma = th.shared(0.5,'sigma',allow_downcast=True)
        this.n_rot = n_rot
        this._input = T.matrix("input")
        this._rot_input = T.dvector("rot")
        this._lr_input = T.scalar("eta")
        this._momentum  = T.scalar("momentum")

        this.input_size = input_size
        this.hidden_size = hidden_size
        this.number_channel = ch

        this.training_set = None

        if (this.UseTensor):
        	this.Momentum['nablaW'] = th.shared(np.zeros((input_size,hidden_size,n_rot)),'nablaW',allow_downcast=True)
        else:
        	this.Momentum['nablaW'] = th.shared(np.zeros((input_size,hidden_size)),'nablaW',allow_downcast=True)

        this.Momentum['nablab'] = th.shared(np.zeros((hidden_size,)), 'nablab', allow_downcast=True)
        this.Momentum['nablac'] = th.shared(np.zeros((input_size,)), 'nablac', allow_downcast=True)

        this.R = [None]*n_rot

        this.ClassPrior = np.asarray([1.0/n_rot]*n_rot)

        angles = np.deg2rad( np.arange(0,360,360.0/n_rot) )
        for t in range(n_rot):
            this.R[t] = RotationMatrix(ws,angles[t])
            if (ch>1):
                N = ws**2
                tmp = np.zeros((N*ch,N*ch))
                for j in range(ch):
                    tmp[j*N:(j+1)*N,j*N:(j+1)*N] = this.R[t]
                this.R[t] = tmp



    def _preactivation_ph_x(this,x,r,real=True):
        W = this._subtensorW[r] if this.UseTensor == True else T.dot(this.R[r],this.W)
        if (real):
            W =W / this.sigma

        return T.dot(x,W) + this.b


    def _ph_x(this,x,rot,real=True):
        h = 0

        for i in range(this.n_rot):
            h+= T.nnet.sigmoid(this._preactivation_ph_x(x,i,real)) * T.eq(i,rot).dimshuffle(0,'x')

        return h


    def _preactivation_px_h(this,h,r,real=True):
        W = this._subtensorW[r] if this.UseTensor == True else T.dot(this.R[r],this.W)
        if (real):
            W = W * this.sigma

        return T.dot(h,W.T) + this.c


    def _px_h(this,h,rot,real=True):
        x = 0

        phi = lambda y : y

        if (not real):
            phi = T.nnet.sigmoid

        for i in range(this.n_rot):
            x+= phi(this._preactivation_px_h(h,i,real)) * T.eq(i,rot).dimshuffle(0,'x')

        return x

    def _grad_cd(this,x,rot,k=1,real=True):
        this.real = real


        R = RandomStreams(seed=47)
        x_pos = x
        h_pos = this._ph_x(x,rot,real)
        #x_pos = this._px_h(h_pos,rot,real)

        h_pos_s = R.binomial(h_pos.shape,1,h_pos)

        for t in range(k):
            x_neg = this._px_h(h_pos_s,rot,real)

            #if (not real):
            #    x_neg = R.binomial(x_neg.shape,1,x_neg)

            h_neg = this._ph_x(x_neg,rot,real)

            if (t<(k-1)):
                h_neg = R.binomial(h_neg.shape,1,h_neg)

        b_size = x.shape[0]

        b_grad = T.sum(h_pos - h_neg,axis=0)/b_size
        c_grad = T.sum(x_pos - x_neg,axis=0)/b_size



        #W_pos = T.tensor3(); W_neg = T.tensor3()
        W_pos = T.zeros_like(this.W); W_neg = T.zeros_like(this.W)
        nablaO=T.zeros_like(this.W); nablaITR = T.zeros_like(this.W)
        costMasked = 0

        if (this.UseTensor == True):
        	subW = [this.W[...,k] for k in range(this.n_rot)]
        else:
        	subW = [T.dot(this.R[k].T,this.W) for k in range(this.n_rot)]

        for t in range(0,this.n_rot):
            for tt in range(0, this.n_rot):
                if (t!=tt):
                    costMasked+=(T.dot(subW[t].T,subW[tt]) * (1-T.eye(this.hidden_size,this.hidden_size)))**2

        costMasked = T.mean(0.5*costMasked)
        nablaMasked = [T.grad(costMasked,var) for var in subW]


        this._orthnormal_cost_value = costMasked
        this._itr_cost_value=T.zeros(1)

        for t in range(this.n_rot):
            mask = T.eq(rot,t).nonzero()[0]

            xx = x_pos[mask,:]
            hh = h_pos[mask,:]

            ##orthnormal
            #subW = this._subtensorW[t]
            #orth_cost = this._mask_orth(subW)
            #this._orthnormal_cost_value+=orth_cost
            #nO = T.grad(orth_cost,subW)
            #nablaO = T.set_subtensor(nablaO[:,:,t],nO)
            ##end orthnormal

            ##information theoretic
            #cross_entropy_cost = this._inf_theor_cost(hh)
            #this._itr_cost_value+=cross_entropy_cost
            #gradITR = T.grad(cross_entropy_cost,subW)
            #nablaITR = T.set_subtensor(nablaITR[:,:,t],gradITR)
            ##end itr

            if (this.UseTensor):
            	W_pos = T.inc_subtensor(W_pos[:,:,t],  T.dot(xx.T, hh) )
            else:
            	#counter rotation on purpose
            	W_pos = W_pos + T.dot(this.R[t].T,T.dot(xx.T, hh))

            xx = x_neg[mask, :]
            hh = h_neg[mask, :]

            if (this.UseTensor):
            	W_neg = T.inc_subtensor(W_neg[:, :, t], T.dot(xx.T, hh))
            else:
            	#counter rotation on purpose
            	W_neg = W_neg + T.dot(this.R[t].T,T.dot(xx.T, hh))

        W_grad = (W_pos - W_neg)

        m_idx = np.arange(this.n_rot)

        W_grad_shared = T.zeros_like(W_grad)

        if (this.SharingGradients) and (this.UseTensor):
            for t in range(this.n_rot):
                for tt in range(this.n_rot):
                    #pos.vishid(:,:, qq) = pos.vishid(:,:, qq) + Tlist(:,:, k)' * old_pos(:,:,m_idx(k));
                    W_grad_shared = T.inc_subtensor(W_grad_shared[:,:,t], T.dot(this.R[m_idx[tt]].T,W_grad[:,:,tt]))
                    nablaO = T.inc_subtensor(nablaO[...,t],T.dot(this.R[m_idx[tt]].T,nablaMasked[tt]))
                    print "T_%d(W_%d)" % (m_idx[tt],tt)
                print ""

                m_idx = np.roll(m_idx,1)

            W_grad_shared = W_grad_shared / b_size

        if ((this.SparsityLambda>0) and (this.SparsityTarget>0)):
            h = T.sum(h_pos,axis=0) / b_size
            tmp = this.SparsityTarget - h
            hmh = h_pos * (1-h_pos)
            dh_reg = tmp* T.sum(hmh,axis=0) / b_size
        else:
            dh_reg = 0

        #

        if (this.ClassRegulariser>0):
            rec_err = T.zeros((x.shape[0],this.n_rot))

            for t in range(this.n_rot):
                rot_vec = t*T.ones((x.shape[0],))
                go_up = this._ph_x(x,rot_vec)
                go_down = this._px_h(go_up,rot_vec)
                rec_err = T.set_subtensor(rec_err[:,t],T.mean((x-go_down)**2,axis=1))

            M = T.sum(rec_err,axis=1,keepdims=True)
            prob = T.mean(T.nnet.softmax(M-rec_err),0)

            class_regulariser = T.sum(this.ClassPrior * T.log2(this.ClassPrior/prob))

            #for i in range(this.n_rot):
            nablaCR = th.grad(class_regulariser,[this.W])[0]
        else:
            nablaCR = T.zeros_like(this.W)




        newGradW = this._momentum * this.Momentum['nablaW'] + (W_grad_shared -
                                                               this.L2Regulariser * this.W -
                                                               (this.OrthogonalRegulariser*nablaO) -
                                                               (this.InformationTheoreticRegulariser * nablaITR) -
                                                               (this.ClassRegulariser * nablaCR)
                                                               ) * this._lr_input
        newGradb = this._momentum * this.Momentum['nablab'] + (b_grad + (this.SparsityLambda*dh_reg)) * this._lr_input
        newGradc = this._momentum * this.Momentum['nablac'] + (c_grad) * this._lr_input


        return [ (this.W,this.W + newGradW),
                 (this.b,this.b + newGradb),
                 (this.c,this.c + newGradc),
                 (this.Momentum['nablaW'], newGradW),
                 (this.Momentum['nablab'], newGradb),
                 (this.Momentum['nablac'], newGradc)]

    def _orthnormal_cost(this,W,x):
        return  ifelse(T.neq(x.size,0),  T.mean( T.sum( (T.dot(T.dot(x, W), W.T) - x) ** 2,axis=1) * 0.5), np.asarray(0.,dtype=np.float64) )
        #return(t ifelse(T.neq(x.size, 0), T.mean( ( T.dot(W.T,W ) - T.eye(this.hidden_size,this.hidden_size) )** 2) * 0.5, np.asarray(0., dtype=np.float64))

    def _inf_theor_cost(this,h):
        return - T.nnet.binary_crossentropy(h,h).sum(axis=1).mean()

    def _mask_orth(this,W):
        return T.mean( (T.dot(W,W.T) * T.eye(this.input_size,this.input_size))**2 )

    def compile(this,real=True,shared=False):
        if(shared):
            this.training_set = th.shared(np.asarray([[0.]]),'Training set',borrow=True)

        if (this.UseTensor):
        	this._subtensorW = [this.W[:, :, t] for t in range(this.n_rot)]
        else:
        	this._subtensorW = [T.dot(this.R[k].T,this.W) for k in range(this.n_rot)]
        #reconstruction
        rot_input = this._rot_input

        up = this._ph_x(this._input,rot_input,real)
        down = this._px_h(up,rot_input,real)

        reconstruction_error = T.mean((this._input - down)**2,axis=1)
        sparsity = T.mean(up)

        grads = this._grad_cd(this._input,rot_input,real=real)


        #weight.sigma = weight.sigma * (1 - eta_sigma) + eta_sigma * sqrt(sum(rec_err_epoch) / nbatch);

        this.reconstruction = th.function([this._input, rot_input], down,allow_input_downcast=True)
        this.features = th.function([this._input, rot_input], up, allow_input_downcast=True)
        this.update = th.function([this._input,rot_input,this._lr_input,this._momentum],[down,reconstruction_error,sparsity,this._orthnormal_cost_value,this._itr_cost_value],updates=grads,allow_input_downcast=True)



    def get_best_rotation(this,x):
        rec = np.zeros((x.shape[0],this.n_rot))

        for i in range(this.n_rot):
            rr = np.ones((x.shape[0],)) * i
            rec[:,i] = np.mean((x - this.reconstruction(x,rr))**2,axis=1)

        return np.argmin(rec,axis=1), np.min(rec,axis=1)

    def get_best_rotation_debug(this, x):
        rec = np.zeros((x.shape[0], this.n_rot))

        for i in range(this.n_rot):
            rr = np.ones((x.shape[0],)) * i
            rec[:, i] = np.mean((x - this.reconstruction(x, rr)) ** 2, axis=1)

        return np.argmin(rec, axis=1), np.min(rec, axis=1),rec



    def train(this,x,rot=None,display=False):
        N = x.shape[0]
        n_batch = int(np.ceil(float(N)/float(this.MiniBatchSize)))

        if (display):
            im=None
            plt.ion()
            fig = plt.figure()




        for t in range(this.Epochs):
            if (this.TrackRotations is not None):
                est_rots = this.get_best_rotation(x)[0].tolist()
                line = ("#%d:"%t)+ ",".join([str(digit) for digit in est_rots])
                with open(this.TrackRotations,'a') as handle:
                    handle.write(line+"\n")

            pbar = pb.ProgressBar(widgets=['Epoch %d/%d: ' %(t+1,this.Epochs),pb.Bar(left='[',right=']'),' ',
                                           pb.Percentage(),pb.ETA(),pb.DynamicMessage('Error'),' ',
                                           pb.DynamicMessage('Sparsity'),'',
                                           pb.DynamicMessage('OrthRegCost'),'',
                                           pb.DynamicMessage('ITRCost')],
                                  maxval=n_batch,fd=stdout)
            pbar.start()

            idx = np.random.permutation(N)

            epoch_error = 0
            epoch_sparsity = 0
            epoch_o_cost = 0
            epoch_itr_cost = 0

            lr = this.Eta / (1+this.EtaDecay*(t+1))

            momentum = this.Momentum['initial'] if t<5 else this.Momentum['final']

            for j in range(n_batch):
                #print j
                b_idx = idx[ this.MiniBatchSize *j : min(this.MiniBatchSize * (j+1),N) ]
                input = x[b_idx,:]



                if (rot is not None):
                    rots = rot[b_idx]
                    #d = this.reconstruction(input,rots)
                    #rec_error = np.mean((input - d)**2,axis=1)
                else:
                    rots, rec_error = this.get_best_rotation(input)

                #print rots ; print this.mmmm.eval({this._rot_input:rots})
                #print '[%.5f]' % abs(this.W.get_value()).sum()

                rec, rec_error, sparsity, o_cost, itr_cost = this.update(input, rots, lr, momentum)

                mean_error = np.mean(rec_error)

                if (this.real):
                    sigma = this.sigma.get_value()
                    this.sigma.set_value( sigma * (1 - this.EtaSigma) + (this.EtaSigma) * np.sqrt(mean_error))


                epoch_error += mean_error
                epoch_sparsity += sparsity
                epoch_o_cost += o_cost.tolist()
                epoch_itr_cost += itr_cost

                pbar.update(j+1,Error=epoch_error/(j+1),Sparsity=epoch_sparsity/(j+1),OrthRegCost=epoch_o_cost/(j+1),ITRCost=epoch_itr_cost/(j+1))

            if (display):
                ws = np.round(np.sqrt(this.input_size/this.number_channel)).astype('int')
                data = rec[0,:].reshape((ws,ws))
                if (im is None):
                    im = plt.imshow(data,cmap='gray')
                else:
                    im.set_data(data)
                plt.draw()


            pbar.finish("[DONE]\n")

    # def train_classifier(this,x,y,xt=None,yt=None):
    #     N = x.shape[0]
    #     n_batch = int(np.ceil(float(N)/float(this.MiniBatchSize)))
    #
    #     for t in range(this.Epochs):
    #         pbar = pb.ProgressBar(widgets=['Epoch %d/%d: ' %(t+1,this.Epochs),pb.Bar(left='[',right=']'),pb.Percentage(),pb.ETA()],maxval=n_batch,fd=stdout)
    #         pbar.start()
    #
    #         idx = np.random.permutation(N)
    #
    #         for j in range(n_batch):
    #             b_idx = idx[ this.MiniBatchSize *j : min(this.MiniBatchSize * (j+1),N) ]
    #             input = x[b_idx,:]
    #             yy = y[b_idx]
    #
    #             _,cost = this.f_train_class(input,yy)
    #
    #             pbar.update(j+1)
    #
    #         y_hat_train = this.test(x,False)
    #         acc_train = performance(y,y_hat_train)
    #
    #         report = '\tTrain Error: %.2f' % acc_train
    #
    #         if ((xt is not None) and (yt is not None)):
    #             y_hat_test = this.test(xt,False)
    #             acc_test = performance(yt,y_hat_test)
    #
    #             report += '\tTest Error: %.2f' %acc_test
    #
    #         pbar.finish('\tCost: %.5f%s\n'%(cost.item(),report))

    def predict(this,x,rot=None):
        if (rot is None):
            rot,_ = this.get_best_rotation(x)
        h = this.features(x,rot)
        return h


    def test(this,x,rot=None,full=False):
        if full:
            h = this.predict(x,rot)
        else:
            M = x.shape[0]
            n_batch = int(np.ceil(float(M)/float(this.MiniBatchSize)))

            h = None

            for k in range(n_batch):
                s = slice(k*this.MiniBatchSize, min((k+1)*this.MiniBatchSize,M))

                h_ = this.predict(x[s,:],rot[s] if rot is not None else None)

                if (h is None):
                    h = h_
                else:
                    h = np.concatenate((h,h_))

        return h

    def save(this,fname):
        params = [x.get_value() for x in [this.W,this.b,this.c,this.sigma]]
        np.savez(fname, theta=params)

    def load(this,fname):
        params = np.load(fname)['theta']

        this.n_rot = params[0].shape[2]

        this.W.set_value(np.asarray(params[0], dtype='float32'))
        this.b.set_value(np.asarray(params[1], dtype='float32'))
        this.c.set_value(np.asarray(params[2], dtype='float32'))
        this.sigma.set_value(np.asarray(params[3], dtype='float32'))


class IRIRBM_SVM(IRIRBM):

    def svm_trained(this):
        try:
            a = this.svm_model
            return True
        except:
            return False

    def train(this,x,rot,display=None):
        this.train_svm(x,rot)
        super(IRIRBM_SVM,this).train(x,rot=None,display=display)

    def train_svm(this,x,rot):
        xx = x.tolist()
        rr = rot.tolist()
        training_problem = svm_problem(rr, xx)
        params = svm_parameter('-s 0 -t 2 -g 0.02')

        this.svm_model = svm_train(training_problem, params)


    def get_best_rotation(this,x):
        xx = x.tolist()
        yy = [0] * len(xx)
        rot = svm_predict(yy,xx,this.svm_model)[0]
        return np.asarray(rot,'uint8'),None

    def get_best_rotation_debug(this, x):
        return this.get_best_rotation(x)

    def test(this, x, full=False):
        return super(IRIRBM_SVM,this).test(x,None,full=full)

    def save(this,fname):
        params = [x.get_value() for x in [this.W,this.b,this.c,this.sigma]]
        np.savez(fname, theta=params)
        svm_save_model(fname+".svm",this.svm_model)


    def load(this,fname):
        handle = np.load(fname)
        params = handle['theta']

        this.n_rot = params[0].shape[2]

        this.W.set_value(np.asarray(params[0], dtype='float32'))
        this.b.set_value(np.asarray(params[1], dtype='float32'))
        this.c.set_value(np.asarray(params[2], dtype='float32'))
        this.sigma.set_value(np.asarray(params[3], dtype='float32'))

        try:
            this.svm_model = svm_load_model(fname + ".svm")
        except:
            warn("SVM model not saved. SVM training suggested")

def performance(y,yh):
    return np.mean(y==yh)
