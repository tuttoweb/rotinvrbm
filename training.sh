#*******************************************************
# * Valerio Giuffrida <v.giuffrida@ed.ac.uk> 2019
# * 
# * This file is part of the project ROTATION INVARIANT RESTRICTED BOLTZMANN MACHINE
# * 
# * This code is released under GPL v3. Please cite the reference paper when using it.
# *******************************************************

#!/bin/bash

PYTHON=python

${PYTHON} main_my_mnist_rot.py -m my_mnist_rot.npz --train --test -e 100 -C 100 > mnist_output.txt

${PYTHON} main_mpeg.py -m mpeg.npz --train --test -e 100 -C 100 > mpeg_output.txt

${PYTHON} main_zalando.py -m zalando.npz --train --test -e 100 -C 100 > zalando_output.txt
